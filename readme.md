# SNBXHSIID

Dokumentasi perjalanan ana selama mengikuti track pembelajaran Next.js - [Sandbox HSI 3.0](https://sandbox.hsi.id).

Proyek ini berisi kumpulan tugas, demo, dan mini project hasil pembelajaran yang telah ana jalani.

## Pengembang

[Yusoof Moh](https://yusoofsh.id)

## Lisensi

[MIT](https://choosealicense.com/licenses/mit)
