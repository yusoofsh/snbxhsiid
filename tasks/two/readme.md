# Tugas Level 2

Bismillah,

Berikut submisi tugas implementasi desain website ana:

## Poin acceptance criteria

- [ ] Source code hanya terdiri dari HTML, CSS dan JS file. (Catatan: Source code tercampur dengan .svg, .png, .mp4, .tsx)
- [x] Tidak diperbolehkan menggunakan library apapun kecuali React.
- [x] Tidak diperbolehkan menggunakan framework (seperti Next dan Gatsby) atau frontend tool (seperti Webpack, Parcel, dan Vite).
- [x] Tidak diperbolehkan juga menggunakan boilerplate, seperti CRA.
- [x] Ikuti desain yang telah disediakan.
- [x] Pada tahap pertama, user diminta untuk mengisi nama, email, no hp dan perusahaan.
  - [x] Setiap field wajib diisi, ketika user klik tombol “Next Step” dan ada field yang belum diisi maka tampilkan pesan error pada field yang belum diisi tersebut.
  - [x] Email yang digunakan merupakan email yang disediakan oleh gmail, harus berakhiran “@gmail.com”, jika email tidak sesuai format maka tampilkan pesan error “Email is invalid”.
  - [x] No hp harus diawali dengan “08” dan jumlah angka selanjutnya minimal 8 dan maksimal 12, jika no hp tidak sesuai format maka tampilkan pesan error “Phone number is invalid”.
  - [x] Jika user klik tombol “Next Step” dan terdapat field yang memiliki pesan error maka atur fokus pada field tersebut.
- [x] Pada tahap kedua, user diminta untuk memilih salah satu servis yang terdiri dari “Development”, “Web Design”, “Marketing” dan “Other”. Pada tahap ini pilihan “Development” sudah langsung terpilih.
- [x] Pada tahap ketiga, user diminta untuk memilih salah satu project budget yang terdiri dari “5000 - 10000”, “10000 - 20000”, “20000 - 50000” dan “50000 +”. Pada tahap ini pilihan “50000 +” sudah langsung terpilih.
- [x] Pada tahap keempat, user diminta untuk mensubmit data yang sudah diinput sebelumnya.
  - [x] Saat user klik submit maka munculkan alert yang berisi data yang sudah diisi dan dikonversi menjadi string.
- [x] Sebelum submit, user bisa melihat isian yang sudah diisi dari masing-masing tahap dengan mengklik tombol “Previous Step”.
- [x] Tidak boleh menggunakan web store api, seperti local storage dan session storage.

## Hasil pengerjaan

- [Live preview](https://snbxhsiid.yusoofsh.id/0dcc4edd1eb267f52d06279769b2e9af94ead403/tasks/two)
- [Source code](https://gitlab.com/yusoofsh/snbxhsiid/-/tree/0dcc4edd1eb267f52d06279769b2e9af94ead403/tasks/two)

![Tampilan demo](./demo.mp4)

---

Sekian. Jazaakallaahu khairan katsiran.
