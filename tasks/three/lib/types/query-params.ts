export default interface QueryParams {
  [key: string]: string | number | undefined;
}
