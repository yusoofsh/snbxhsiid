# Tugas Level 1

Bismillah,

Berikut submisi tugas implementasi desain website ana:

## Poin acceptance criteria

- [x] Ikuti desain yang telah disediakan di Figma.
- [x] Gunakan aset yang telah disediakan di Google Drive.
- [x] Tambahkan font "Playfair Display" dan "Poppins".
- [x] Hindari penggunaan property CSS position.
- [x] Source code terdiri dari HTML dan CSS saja.

## Hasil pengerjaan

- [Live preview](https://snbxhsiid.yusoofsh.id/662b36e63732ea0d0aee06562c211c221190ce5b/tasks/one)
- [Source code](https://gitlab.com/yusoofsh/snbxhsiid/-/tree/662b36e63732ea0d0aee06562c211c221190ce5b/tasks/one)

![Tampilan utama](./fauxica.png)

---

Sekian. Jazaakallaahu khairan katsiran.
